---
title: "Components"
date: 2019-05-06T16:05:03+02:00
draft: false
description: Learn how Tplus works
---

As seen from user perspective, Tplus is split into 3 parts, those are


* The GUI
* Command Line Tools
* TPlus Deamon

### Tplus Deamon

The deamon is the main process of Tplus, and is in charge of applying the state that is defined in the Storage File.
Whenever you execute a tplus CLI command link for example

```
tplus env create NewEnv
```

All the cli tool does is make changes to the storage file, the daemon will read those changes and apply them,
this also means that the tplus cli on its own wont be able to make changes to running environments, or start a new one.


If you start the GUI with

```
$ tplus gui
```

This will also start the daemon and you dont have to worry about this,
however, if you *only* want to use the cli, you have to start the daemon manually with

```
$ tplus daemon start
```

if it is running, you can check the logs with:

```
$ tplus daemon logs
Starting Damon with pid:  20730
time="2019-05-06T16:00:57+02:00" level=info msg="[Starting DevApi Manager]" source="dev_api_mananger"
time="2019-05-06T16:00:57+02:00" level=info msg="[Starting manager thread for  MyBoxNew]" source="dev_api_mananger"
time="2019-05-06T16:00:57+02:00" level=info msg="[Starting manager thread for  MyBox]" source="dev_api_mananger"
time="2019-05-06T16:00:57+02:00" level=info msg="[Starting DockerManager]" source="docker_network_manager"
..... 

```

The entire source code of the Tplus Daemon is in this repository:

https://gitlab.com/tulpenhaendler/tplus

### Tplus GUI

The Tplus GUI is an Electorn app that renders a VueJs project.

The source code can be found here:
https://gitlab.com/tulpenhaendler/TPlusUI

You can start it with:

```
$ tplus gui
```

Note that this will also start the Tplus Deamon and will throw an error if another daemon process is already running.

### Tplus CLI

```
$ tplus
Usage:
   [command]

Available Commands:
  active      Show active Environment
  daemon      Manage the TezTool Daemon
  env         Manage Environments
  gui         Start Gui and all Dependencies
  help        Help about any command
  init        Setup all Denpendencies
  tools       Tools related to Tplus
  use         Set active Environment

Flags:
  -h, --help   help for this command

Use " [command] --help" for more information about a command.
```

In general, you can use the CLI tool instead of the GUI, as both these components implement the same functionality,
in addition to that, the CLI currently supports more functionality specifically for smart contract developers,
check the CLI documentation for more information.
