---
title: "Variables"
date: 2019-05-06T16:05:03+02:00
draft: false
description: Where can i find that private key again?
---

You can use Variables in your tast files like this:

```md
 {{</* variableName */>}}
```

The following variables will be substituted for environment specific values:

Variable       | Content
---------------|------
env.name       | Name of the Environment
tezosnode      | rpc endpoint ( IP:PORT )
tezosnode.ip   | tezos IP
tezosnode.rpc  | RPC Port number


In Addition to that you can specify Variables to ask for when the task is run, like this:



```
/*
    Format: var <name> <type> <Description>
*/

var name    string    Name
var amount  amount    Amount
var source  address   Address

------------------------------------------------------------------

in alpine run:

    echo "{{ name }} {{ amount }} {{ source.hash }}"

```



And for every Address you define in the Variables section of your task,
Tplus will provide the following Variables:


Variable        | Content           
----------------|------             
(address).pub   | Public Key
(address).priv  | Secret Key
(address).hash  | Hash          
(address).name  | Name   
