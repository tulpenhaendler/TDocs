---
title: "Task Files"
date: 2019-05-06T16:05:03+02:00
draft: false
description: make your Life easier
---


{{%children style="h4" description="true" %}}


## Task Introduction

Task files are Helpers that let you automate Tasks, for example deploying contracts or
sending transactions.

Tasks will always be executed in your currently active Environment,
you can set your active Environment with:

```
$ tplus use <EnvironmentName>
```


TPlus will look for task files in your current directory in the folder `tasks`,
you can execute tasks in that folder by typing:

```
$ tplus <taskname>
```

#### Example

A very, very simple task file might look like this:

```
/*
    Name: Simple Task
    Description: Hello World
*/

var name    string    Your Name

------------------------------------------------------------------

in alpine run:

echo "Hello {{name}}!!"
```

If you put this into the file `./tasks/echo`, tplus will recognise this file and the task will show up in the help outut of the
tplus command:

```
$ tplus
Usage:
[command]

Available Commands:
active      Show active Environment
daemon      Manage the TezTool Daemon
echo        Hello World
env         Manage Environments
....
...
```
    
If you execute this, It will ask for the Variables defined in the var section, and start a
Container with the defined image ( in this case alpine linux ) and execute the echo command.

```
$ tplus echo
? Your Name  Zoë Maya Castillo
___________________________________________________________

Hello Zoë Maya Castillo!!
```

Note that even though Tplus runs each Task in a different Container, the commands are executed in the current
directory, so you can use every command in the container as if you would use them locally.

#### So why is this better than bash scripts?

The above is a very naive, useless example just to illustrate how Tasks work,
In generall, Tasks allow you to interact with the Tezos Node and contract-language specific tools
in a way that abstracts away the specific implementation of how Tplus manages your Environment, and lets you use tools
like language compilers without bothering how to compile/install them. 

For example, this is a Task File that executes a wget command against the Tezos Node of your current Environment


```
/*
    Name: head
    Description: Tezos RPC - get head
*/

------------------------------------------------------------------

in alpine run:

    wget http://{{tezosnode}}/chains/main/blocks/head
    &&
    cat head

```


will output: 

```
$ tplus head
___________________________________________________________

Connecting to 172.28.0.2:18731 (172.28.0.2:18731)
head                 100% |********************************|  1719  0:00:00 ETA

{"protocol":"PsddFKi32cMJ2qPjf43Qv5GDWLDPZb3T3bF6fLKiF5HtvHNU7aP","chai......

```
    
    
Tplus will look for variables like `{{tezosnode}}` in your Task and replace that value with the specific tezos node endpoint relevant to your
current environment, so you dont have to deal with ports, ip addresses or sourcing your bash environment variables every time you switch your environments

Check the following docs to see learn more interesting use cases:

{{% children style="h4" description="true" %}}

