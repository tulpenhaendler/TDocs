---
title: "Images and Commands"
date: 2019-05-06T16:05:03+02:00
draft: false
description: see what Tasks can do
---


You can specify what image to use with

```
in <image> run:
    <command>
```