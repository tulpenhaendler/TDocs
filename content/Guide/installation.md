---
title: "Installation"
date: 2019-05-06T15:42:46+02:00
draft: false
description: Install Tplus
---

The following Guide will work on any Linux Distribution, such as Ubuntu, Debian, Mx Linux, etc.

### Prerequisites

TPlus needs docker installed on your system, since all Tezos Nodes and task containers run in an isolated docker
environment, the easiest way to see if docker is working is this command:

```
docker run hello-world

Hello from Docker!
This message shows that your installation appears to be working correctly.
```

In case it is not installed, on most Linux Distributions this command is sufficient to install docker:

```
curl -L get.docker.com | bash
```

After this script has finished, have a look at its output to make sure your current user
has permissions to create docker containers.

### Precompiled Binary

The Tplus binary is build on every push to the master branch on gitlab, and can be downloaded and
installed like this:

```
wget https://s3.amazonaws.com/tplus.dev/tplus/build/tplus
sudo cp tplus /usr/local/bin/tplus && sudo chmod +x /usr/local/bin/tplus && rm tplus
```

### Build from Source

This needs Go installed on your System,
for instructions on how to install go, please go to:

https://golang.org/doc/install


```
git clone https://gitlab.com/tulpenhaendler/tplus
cd tplus
go build *.go
```

This will build a `./main` binary in your current directory,
to use it system-wide just copy it into your bin folder


```
sudo cp ./main /usr/local/bin/tplus
```